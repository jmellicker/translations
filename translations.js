{
  pages: {
    home: {
      title: "Connecting people to lawyers",
      caption: "Over 80% of people involved in the legal system today have no legal representation. LawCo is a legal platform that connects people in need of legal help with qualified lawyers in a secure, easy-to-use environment across web and mobile devices.",
      findOutMoreAbout: "Find out more about LawCo: ",
      findOutMore: "find out more",
      forClients: "For Consumers",
      forClientsDescription: "Do you have a court case? Do you need legal advice?",
      forLawyers: "For Lawyers",
      forLawyersDescription: "Looking to build your book of business?",

      aboutTitle: "Access to justice should be simple!",
      aboutCaption: [
        "Created and founded by Chicago native, lawyer Lauren Glennon, LawCo provides people with access to qualified lawyers. It also provides lawyers with a new method of lead generation. The LawCo platform is currently available in Cook County, IL. LawCo is scheduled to launch nationally in 2020.",
        "Get notified when LawCo comes to a city near you!",
      ],
      linksTitle: "As seen in:",
      links: [
        {
          label: "Chicago Inno",
          image: logoChicagoInno,
          url: "https://www.americaninno.com/chicago/chicago-startup/on-demand-attorneys-this-chicago-startup-is-making-it-a-reality/",
        },
        {
          label: "Crains Chicago Business",
          image: logoCrainsChicagoBusiness,
          url: "https://www.chicagobusiness.com/law/rules-complicate-matching-lawyers-potential-clients",
        },
        {
          label: "Daily Herald",
          image: logoDailyHerald,
          url: "https://www.dailyherald.com/submitted/20181217/lawyers-on-demand-yes-please-lawco",
        },
      ],
      footerHeadings: [
        'Do you need a lawyer?',
        'Are you a lawyer looking for more leads?'
      ],
      footerCaption: 'Login to the LawCo website or download the app and connect today!',
      appButtons: [
        {
          image: appStoreButtonImage,
          url: 'https://apps.apple.com/us/app/lawco/id1244700591',
        },
        {
          image: googlePlayButtonImage,
          url: 'https://play.google.com/store/apps/details?id=com.SnapMobile.LawyerUp',
        }
      ],
      socialLinks: [
        {
          icon: 'fab fa-facebook-square',
          url: 'https://www.facebook.com/getlawco/',
        },
        {
          icon: 'fab fa-instagram',
          url: 'https://www.instagram.com/get_lawco/',
        },
        {
          icon: 'fab fa-twitter',
          url: 'https://twitter.com/getlawco',
        },
        {
          icon: 'fab fa-linkedin',
          url: 'https://www.linkedin.com/company/get-lawco-llc/',
        },
      ]
    },
    lawyers: {
      title: "Get free access to a stream of potential new clients.",
      registerNow: "Register now",
      posts: [
        {
          title: "Filter open cases by location or type.",
          caption: "Search for cases that are a good fit for your practice."
        },
        {
          title: "Email or text notifications.",
          caption: "Get notified about new cases as soon as they're posted."
        },
      ]
    },
    clients: {
      title: "Get instant access to legal help. Click below to get started!",
      posts: [
        {
          title: "Enter your court date",
          caption:
            "Have a court date? Enter it into LawCo and find legal representation."
        },
        {
          title: "Get legal advice.",
          caption:
            "Have a legal problem or case? Get advice from local lawyers."
        }
      ]
    },
    signInUp: {
      hint: {
        anonymous: "Enter your email address to begin:",
        newUser: [
          "Welcome to LawCo!",
          "Please fill out the following information:"
        ],
        exists: [
          `Welcome back, {firstName}!`,
          "Enter your password to sign in:"
        ]
      },
      resetPasswordButton: "reset password",
      emailSent: "Your reset password email has been sent. Please check your inbox for instructions!",
      resetPassword: "Set new password for {0}",
      resetPwNoEmail: "Set new password for your account",
      lawyerConsent: "I accept my affirmative duty to update LawCo of any change in registration status with the ARDC.*",
      passwordHint: "Password must be at least 6 characters long with one special character.",
      readAndAccept: 'I have read and accept the {termsAndConditions}.*',
      termsAndConditions: 'Terms And Conditions',
      clientConsents: [
        {
          title: "All Illinois Attorneys in Good Standing May Use the LawCo App.",
          description: "LawCo does not limit the number or type of attorneys who are eligible to use the App. Participating attorneys are only screened to ensure they are actively admitted in Illinois, and are in good standing with no history of discipline.*"
        },
        {
          title: "Attorneys Pay LawCo for the Opportunity to Contact You.",
          description: "Although the LawCo App is free for you to use, any attorney who wants to respond to your request is required to pay a fee to LawCo. Despite the attorney’s payment, you are not obligated to retain any attorney who responds to you through the App.*"
        },
        {
          title: "No Attorney-Client Relationship with LawCo.",
          description: "LawCo is not a law firm and cannot give legal advice. Any form of use of the App by you is not intended to and does not create an attorney-client relationship between any person or entity and LawCo. In addition, no communication with LawCo shall create an attorney-client relationship.*"
        }
      ]
    },
    requestLawyer: {
      title: "Request a Lawyer",
      requestCourtDate: "I have a court date",
      requestLegalAdvice: "I need legal advice"
    },
    requestsForLawyer: {
      title: "Requests for Lawyer",
      noResultMsg: {
        requests: 'There are no open requests to display.',
        leads: 'There are no open cases to display.',
        closedLeads: 'There are no closed leads to display.'
      }
    },
    lawyerApplications: {
      title: "Lawyer Applications",
      status: {
        pending: "Pending",
        approved: "Approved",
        rejected: "Rejected"
      }
    },
    faq: {
      title: "FAQ",
      items: {
        lawyer: [
          {
            question: "How long does it take to gain access to the app?",
            answer:
              "It should take a lawyer no longer than 24 hours to gain access to the app."
          },
          {
            question:
              "Do I have to be registered and authorized to practice with the ARDC to be LawCo approved?",
            answer:
              "Yes. You must be registered and authorized to practice in Illinois with the ARDC. Under the Terms and Conditions of the LawCo app, it is also your affirmative obligation to inform LawCo if your registration status changes."
          },
          {
            question: "Are there promo codes available?",
            answer: "Yes. From time to time, promo codes will be available!"
          },
          {
            question: "Can I use the app for more than one potential lead?",
            answer:
              "Absolutely! Please use the app to be connected with a potential lead as often as you would like."
          }
        ],
        client: [
          {
            question: 'Lorem ipsum?',
            answer: 'Bacon ipsum dolor amet salami t-bone bacon leberkas tail shoulder shankle ham filet mignon turkey chuck porchetta sausage. '
          }
        ]
      },
      contact: {
        question: "How can I contact LawCo?",
        answer: "Please email",
        email: "admin@getlawco.com"
      }
    },
    tac: {
      title: "Terms and Conditions",
      legal: "Legal",
      termsOfUse: "U.S. Terms of Use",
      effective: "Effective: January 1, 2018",
      sections: [
        {
          title: "1. Contractual Relationship",
          contents: [
            {
              texts: [
                `These Terms of Use ("Terms") govern your access or use, from within the United States and its territories and possessions, of the applications, websites, content, products, and services (the "Services," as more fully defined below in Section 3) made available in the United States and its territories and possessions by Get LawCo, LLC and its parents, subsidiaries, representatives, affiliates, officers and directors (collectively, "Get LawCo" and the LawCo App, used interchangeably and intermittently). PLEASE READ THESE TERMS CAREFULLY, AS THEY CONSTITUTE A LEGAL AGREEMENT BETWEEN YOU AND GET LAWCO, LLC and the LAWCO APP . In these Terms, the words "including" and "include" mean "including, but not limited to."`,
                "By accessing or using the Services, you confirm your agreement to be bound by these Terms. If you do not agree to these Terms, you may not access or use the Services. These Terms expressly supersede prior agreements or arrangements with you. Get LawCo may immediately terminate these Terms or any Services with respect to you, or generally cease offering or deny access to the Services orany portion thereof, at any time for any reason.",
                "IMPORTANT: PLEASE REVIEW THE ARBITRATION AGREEMENT SET FORTH BELOW CAREFULLY, AS IT WILL REQUIRE YOU TO RESOLVE DISPUTES WITH GET LAWCO ON AN INDIVIDUAL BASIS THROUGH FINAL AND BINDING ARBITRATION. BY ENTER ING THIS AGREEMENT, YOU EXPRESSLY ACKNOWLEDGE THAT YOU HAVE READ AND UNDERSTAND ALL OF THE TERMS OF THIS AGREEMENT AND HAVE TAKEN TIME TO CONSIDER THE CONSEQUENCES OF THIS IMPORTANT DECISION.",
                "Supplemental terms may apply to certain Services, such as policies for a particular event, program, activity or promotion, and such supplemental terms will be disclosed to you in separate region-specific disclosures or in connection with the applicable Service(s). Supplemental terms are in addition to, and shall be deemed a part of, the Terms for the purposes of the applicable Service(s). Supplemental terms shall prevail over these Terms in the event of a conflict with respect to the applicable Services.",
                `Get LawCo may amend the Terms from time to time. Amendments will be effective upon Get LawCo's posting of such updated Terms at this location or in the amended policies or supplemental terms on the applicable Service(s). Your continued access or use of the Services after such posting confirms your consent to be bound by the Terms, as amended. If Get LawCo changes these Terms after the date you first agreed to the Terms (or to any subsequent changes to these Terms), you may reject any such change by providing Get LawCo written notice of such rejection within 30 days of the date such change became effective, as indicated in the "Effective" date above. This written notice must be provided either (a) by mail or hand delivery to our registered agent for service of process, c/o Get LawCo, LLC (the name and current contact information for the registered agent in each state are available online here), or (b) by email from the email address associated with your Account to: admin@getlawco.com. In order to be effective, the notice must include your full name and clearly indicate your intent to reject changes to these Terms. By rejecting changes, you are agreeing that you will continue to be bound by the provisions of these Terms as of the date you first agreed to the Terms (or to any subsequent changes to these Terms). Get LawCo’s collection and use of personal information in connection with the Services is described in Get LawCo's Privacy Statements located at www.getlawco.com.`
              ]
            }
          ]
        },
        {
          title: "2. Arbitration Agreement",
          contents: [
            {
              texts: [
                "By agreeing to the Terms, you agree that you are required to resolve any claim that you may have against Get LawCo on an individual basis in arbitration, as set forth in this Arbitration Agreement. This will preclude you from bringing any class, collective, or representative action against Get LawCo, and also preclude you from participating in or recovering relief under any current or future class, collective, consolidated, or representative action brought against Get LawCo by someone else."
              ]
            },
            {
              title:
                "Agreement to Binding Arbitration Between You and Get LawCo",
              texts: [
                "You and Get LawCo agree that any dispute, claim or controversy arising out of or relating to (a) these Terms or the existence, breach, termination, enforcement, interpretation or validity thereof, or (b) your access to or use of the Services at any time, whether before or after the date you agreed to the Terms, will be settled by binding arbitration between you and Get LawCo, and not in a court of law.",
                "You acknowledge and agree that you and Get LawCo are each waiving the right to a trial by jury or to participate as a plaintiff or class member in any purported class action or representative proceeding. Unless both you and Get LawCo otherwise agree in writing, any arbitration will be conducted only on an individual basis and not in a class, collective, consolidated, or representative proceeding. However, you and Get LawCo each retain the right to bring an individual action in small claims court and the right to seek injunctive or other equitable relief in a court of competent jurisdiction to prevent the actual or threatened infringement, misappropriation or violation of a party's copyrights, trademarks, trade secrets, patents or other intellectual property rights."
              ]
            },
            {
              title: "Rules and Governing Law",
              texts: [
                `The arbitration will be administered by the American Arbitration Association ("AAA") in accordance with the AAA’s Consumer Arbitration Rules and the Supplementary Procedures for Consumer Related Disputes (the "AAA Rules") then in effect, except as modified by this Arbitration Agreement. The AAA Rules are available by calling the AAA.`,
                `The parties agree that the arbitrator (“Arbitrator”), and not any federal, state, or local court or agency, shall have exclusive authority to resolve any disputes relating to the interpretation, applicability, enforceability or formation of this Arbitration Agreement, including any claim that all or any part of this Arbitration Agreement is void or voidable. The Arbitrator shall also be responsible for determining all threshold arbitrability issues, including issues relating to whether the Terms are unconscionable or illusory and any defense to arbitration, including waiver, delay, laches, or estoppel.`,
                `Notwithstanding any choice of law or other provision in the Terms, the parties agree and acknowledge that this Arbitration Agreement evidences a transaction involving interstate commerce and that the Federal Arbitration Act, 9 U.S.C. § 1 et seq. (“FAA”), will govern its interpretation and enforcement and proceedings pursuant thereto. It is the intent of the parties that the FAA and AAA Rules shall preempt all state laws to the fullest extent permitted by law. If the FAA and AAA Rules are found to not apply to any issue that arises under this Arbitration Agreement or the enforcement thereof, then that issue shall be resolved under the laws of the state of Illinois.`
              ]
            },
            {
              title: "Process",
              texts: [
                "A party who desires to initiate arbitration must provide the other party with a written Demand for Arbitration as specified in the AAA Rules. The Arbitrator will be either (1) a retired judge or (2) an attorney specifically licensed to practice law in the state of Illinois and will be selected by the parties from the AAA's roster of consumer dispute arbitrators. If the parties are unable to agree upon an Arbitrator within seven (7) days of delivery of the Demand for Arbitration, then the AAA will appoint the Arbitrator in accordance with the AAA Rules."
              ]
            },
            {
              title: "Location and Procedure",
              texts: [
                "Unless you and Get LawCo otherwise agree, the arbitration will be conducted in the county where you reside. If your claim does not exceed $10,000, then the arbitration will be conducted solely on the basis of documents you and Get LawCosubmit to the Arbitrator, unless you request a hearing or the Arbitrator determines that a hearing is necessary. If your claim exceeds $10,000, your right to a hearing will be determined by the AAA Rules. Subject to the AAA Rules, the Arbitrator will have the discretion to direct a reasonable exchange of information by the parties, consistent with the expedited nature of the arbitration."
              ]
            },
            {
              title: "Arbitrator's Decision",
              texts: [
                "The Arbitrator will render an award within the time frame specified in the AAA Rules. Judgment on the arbitration award may be entered in any court having competent jurisdiction to do so. The Arbitrator may award declaratory or injunctive relief only in favor of the claimant and only to the extent necessary to provide relief warranted by the claimant's individual claim. An Arbitrator’s decision shall be final and binding on all parties. An Arbitrator’s decision and judgment thereon shall have no precedential or collateral estoppel effect. If you prevail in arbitration you will be entitled to an award of attorneys' fees and expenses, to the extent provided under applicable law."
              ]
            },
            {
              title: "Fees",
              texts: [
                "Your responsibility to pay any AAA filing, administrative and arbitrator fees will be solely as set forth in the AAA Rules. Furthermore, you will be responsible for all fees if the Arbitrator finds that either the substance of your claim or the relief sought in your Demand for Arbitration was frivolous or was brought for an improper purpose."
              ]
            },
            {
              title: "Changes",
              texts: [
                `Notwithstanding the provisions in Section I above, regarding consent to be bound by amendments to these Terms, if Get LawCochanges this Arbitration Agreement after the date you first agreed to the Terms (or to any subsequent changes to the Terms), you may reject any such change by providing Get LawCo written notice of such rejection within 30 days of the date such change became effective, as indicated in the "Effective" date above. This written notice must be provided by mail or hand delivery to our registered agent for service of process, c/o Get LawCo, LLC, and in order to be effective, the notice must include your full name and clearly indicate your intent to reject changes to this Arbitration Agreement. By rejecting changes, you are agreeing that you will arbitrate any dispute between you and Get LawCo in accordance with the provisions of this Arbitration Agreement as of the date you first agreed to the Terms (or to any subsequent changes to the Terms).`
              ]
            },
            {
              title: "Severability and Survival",
              texts: [
                "If any portion of this Arbitration Agreement is found to be unenforceable or unlawful for any reason, (1) the unenforceable or unlawful provision shall be severed from these Terms; (2) severance of the unenforceable or unlawful provision shall have no im pact whatsoever on the remainder of the Arbitration Agreement or the parties’ ability to compel arbitration of any remaining claims on an individual basis pursuant to the Arbitration Agreement; and (3) to the extent that any claims must therefore proceed on a class, collective, consolidated, or representative basis, such claims must be litigated in a civil court of competent jurisdiction and not in arbitration, and the parties agree that litigation of those claims shall be stayed pending the outcome of any individual claims in arbitration."
              ]
            }
          ]
        },
        {
          title: "3. The Services",
          contents: [
            {
              texts: [
                `The Services comprise mobile applications and related services (each, an "Application"), which enable users to arrange and schedule a meeting with an attorney, logistics and services, including with third party providers of such services under agreement with Get LawCo or certain of Get LawCo's affiliates ("Third Party Providers"). In certain instances, the Services may also include an option to receive logistics and/or delivery services for an upfront price, subject to acceptance by the respective Third Party Providers. Unless otherwise agreed by Get LawCo in a separate written agreement with you, the Services are made available solely for your personal, noncommercial use.`,
                "YOU ACKNOWLEDGE THAT YOUR ABILITY TO ACCESS AND MEET WITH AN ATTORNEY THROUGH THE USE OF THE SERVICES OF GET LAWCO AND THE LAWCO APP DOES NOT ESTABLISH GET LAWCO OR THE LAWCO APP AS A PROVIDER OF LEGAL ADVICE, LOGISTICS, DOES NOT ESTABLISH AN ATTORNEY-CLIENT PRIVILEGE AND DOES NOT ESTABLISH GET LAWCO OR THE LAWCO APP AS AN INDIVIDUAL ATTORNEY OR COLLECTIVELY AS A LAW FIRM."
              ]
            },
            {
              title: "License",
              texts: [
                "Subject to your compliance with these Terms, Get LawCo grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferable license to: (i) access and use the Applications on your personal device solely in connection with your use of the Services; and (ii) access and use any content, information and related materials that may be made available through the Services, in each case solely for your personal, noncommercial use. Any rights not expressly granted herein are reserved by Get LawCo and Get LawCo's licensors."
              ]
            },
            {
              title: "Restrictions",
              texts: [
                "You may not: (i) remove any copyright, trademark or other proprietary notices from any portion of the Services; (ii) reproduce, modify, prepare derivative works based upon, distribute, license, lease, sell, resell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Services except as expressly permitted by Get LawCo; (iii) decompile, reverse engineer or disassemble the Services except as may be permitted by applicable law; (iv) link to, mirror or frame any portion of the Services; (v) cause or launch any programs or scripts for the purpose of scraping, indexing, surveying, or otherwise data mining any portion of the Services or unduly burdening or hindering the operation and/or functionality of any aspect of the Services; or (vi) attempt to gain unauthorized access to or impair any aspect of the Services or its related systems or networks."
              ]
            },
            {
              title: "Provision of the Services",
              texts: [
                "You acknowledge that portions of the Services may be made available under Get LawCo's various brands. You also acknowledge that the Services may be made available under such brands or request options by or in connection with: (i) certain of Get LawCo's subsidiaries and affiliates; or (ii) independent Third Party Providers."
              ]
            },
            {
              title: "Third Party Services and Content",
              texts: [
                "The Services may be made available or accessed in connection with third party services and content (including advertising) that Get LawCo does not control. You acknowledge that different terms of use and privacy policies may apply to your use of such third-party services and content. Get LawCo does not endorse such third-party services and content and in no event shall Get LawCo be responsible or liable for any products or services of such third party providers. Additionally, Apple Inc., Google, Inc., Microsoft Corporation or BlackBerry Limited will be a third-party beneficiary to this contract if you access the Services using Applications developed for Apple iOS, Android, Microsoft Windows, or Blackberry-powered mobile devices, respectively. These third-partybeneficiaries are not parties to this contract and are not responsible for the provision or support of the Services in any manner. Your access to the Services using these devices is subject to terms set forth in the applicable third party beneficiary's terms of service."
              ]
            },
            {
              title: "Ownership",
              texts: [
                "The Services and all rights therein are and shall remain Get LawCo's property or the property of Get LawCo's licensors. Neither these Terms nor your use of the Services convey or grant to you any rights: (i) in or related to the Services except for the limited license granted above; or (ii) to use or reference in any manner Get LawCo's company names, logos, product and service names, trademarks or services marks or those of Get LawCo's licensors."
              ]
            }
          ]
        },
        {
          title: "4. Access and Use of the Services",
          contents: [
            {
              title: "User Accounts",
              texts: [
                `In order to use most aspects of the Services, you must register for and maintain an active personal user Services account ("Account"). You must be at least 18 years of age, or the age of legal majority in your jurisdiction (if different than 18), to obtain an Account, unless a specific Service permits otherwise. Account registration requires you to submit to Get LawCo certain personal information, such as your name, address, mobile phone number and age, as well as at least one valid payment method supported by Get LawCo. You agree to maintain accurate, complete, and up-to-date information in your Account. Your failure to maintain accurate, complete, and up-to-date Account information, including having an invalid or expired payment method on file, may result in your inability to access or use the Services. You are responsible for all activity that occurs under your Account, and you agree to maintain the security and secrecy of your Account username and password at all times. Unless otherwise permitted by Get LawCo in writing, you may only possess one Account.`
              ]
            },
            {
              title: "User Requirements and Conduct",
              texts: [
                "The Service is not available for use by persons under the age of 18. You may not authorize third parties to use your Account, and you may not allow persons under the age of 18 to receive services from Third Party Providers unless they are accompanied by you. You may not assign or otherwise transfer your Account to any other person or entity. You agree to comply with all applicable laws when accessing or using the Services, and you may only access or use the Services for lawful purposes. You may not, in your use,access or use the Services to cause nuisance, annoyance, inconvenience, or property damage, whether to the Third-Party Provider or any other party. In certain instances, you may be asked to provide proof of identity to access or use the Services, and you agree that you may be denied access to or use of the Services if you refuse to provide proof of identity."
              ]
            },
            {
              title: "Text Messaging and Telephone Calls",
              texts: [
                "You agree that Get LawCo may contact you by telephone or text messages (including by an automatic telephone dialing system) at any of the phone numbers provided by you or on your behalf in connection with an Get LawCo account, including for marketing purposes. You understand that you are not required to provide this consent as a condition of purchasing any property, services. Get LawCo may contact you as outlined in its User Privacy Statement."
              ]
            },
            {
              title: "Referrals and Promotional Codes",
              texts: [
                `Get LawCo may, in its sole discretion, create referral and/or promotional codes ("Promo Codes") that may be redeemed for discounts on future services, or other features or benefits related to the Services and/or a Third-Party Provider's services, subject to any additional terms that Get LawCo establishes. You agree that Promo Codes: (i) must be used for the intended audience and purpose, and in a lawful manner; (ii) may not be duplicated, sold or transferred in any manner, or made available to the general public (whether posted to a public form or otherwise), unless expressly permitted by Get LawCo; (iii) may be disabled by Get LawCo at any time for any reason without liability to Get LawCo; (iv) may only be used pursuant to the specific terms that Get LawCo establishes for such Promo Code; (v) are not valid for cash; and (vi) may expire prior to your use. Get LawCo reserves the right to withhold or deduct credits or other features or benefits obtained through the use of the referral system or Promo Codes by you or any other user in the event that Get LawCo determines or believes that the use of the referral system or use or redemption of the Promo Code was in error, fraudulent, illegal, or otherwise in violation of Get LawCo’s Terms.`
              ]
            },
            {
              title: "User Provided Content",
              texts: [
                `Get LawCo may, in Get LawCo's sole discretion, permit you from time to time to submit, upload, publish or otherwise make available to Get LawCo through the Services textual, audio, and/or visual content and information, including commentary and feedback related to the Services, initiation of support requests, and submission of entries for competitions and promotions ("User Content"). Any User Content provided by you remains your property. However, by providing User Content to Get LawCo, you grant Get LawCoa worldwide, perpetual, irrevocable, transferable, royalty-free license, with the right to sublicense, to use, copy, modify, create derivative works of, distribute, publicly display, publicly perform, and otherwise exploit in any manner such User Content in all formats and distribution channels now known or hereafter devised (including in connection with the Services and Get LawCo's business and on third-party sites and services), without further notice to or consent from you, and without the requirement of payment to you or any other person or entity.`,
                `You agree to not provide User Content that is defamatory, libelous, hateful, violent, obscene, pornographic, unlawful, or otherwise offensive, as determined by Get LawCo in its sole discretion, whether or not such material may be protected by law. Get LawComay, but shall not be obligated to, review, monitor, or remove User Content, at Get LawCo's sole discretion and at any time and for any reason, without notice to you.`,
                "You represent and warrant that: (i) you either are the sole and exclusive owner of all User Content or you have all rights, licenses, consents and releases necessary to grant Get LawCo the license to the User Content as set forth above; and (ii) neither the User Content, nor your submission, uploading, publishing or otherwise making available of such User Content, nor Get LawCo's use of the User Content as permitted herein will infringe, misappropriate or violate a third party's intellectual property or proprietary rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation."
              ]
            },
            {
              title: "Network Access and Devices",
              texts: [
                "You are responsible for obtaining the data network access necessary to use the Services. Your mobile network's data and messaging rates and fees may apply if you access or use the Services from your device. You are responsible for acquiring and updating compatible hardware or devices necessary to access and use the Services and Applications and any updates thereto. Get LawCo does not guarantee that the Services, or any portion thereof, will function on any particular hardware or devices. In addition, the Services may be subject to malfunctions and delays inherent in the use of the Internet and electronic communications."
              ]
            }
          ]
        },
        {
          title: "5. Payment",
          contents: [
            {
              texts: [
                `You understand that use of the Services may result in charges to you for the services you receive ("Charges"). Get LawCo does not receive or enable your payment of the applicable Charges for services obtained through your use of the Services.`,
                "GET LAWCO DOES NOT PROCESS PAYMENTS MADE BY YOU TO THIRD-PARTY PROVIDERS FOR ANY REASON. GET LAWCO ONLY PROCESSES PAYMENTS MADE BY ATTORNEYS TO CONNECT TO A LEAD; THE LEAD GENERATION FEE. GET LAWCO DOES NOT CHARGE THE NON-ATTORNEY USER FOR THE SERVICES USED TO CONNECT WITH AN ATTORNEY.",
                "ANY AND ALL FUNDS DUE AND OWED TO AN ATTORNEY FROM A USER, UPON AND AFTER LEAD CONNECTION, WILL NOT AND ARE NOT PROCESSED BY GET LAWCO OR THE LAWCO APP. THE ATTORNEY HAS THE RIGHT TO CHARGE A LEGAL FEE, THAT ARE COMPLETELY INDEPENDENT OF GET LAWCO AND THE LAWCO APP. GET LAWCO AND THE LAWCO APP ONLY PROVIDE A CONNECTION SERVICE FOR ATTORNEYS TO CONNECT TO LEADS AND VICE-VERSA, FOR USERS TO CONNECT WITH ATTORNEYS. GET LAWCO AND THE LAWCO APP ARE NOT RESPONSIBLE FOR ANY LEGAL ADVICE, SERVICES LEGAL OR OTHERWISE OR OUTCOMES RESULTING FROM THE LEAD CONNECTION SERVICE THEY PROVIDE. GET LAWCO AND THE LAWCO APP ARE NOT AFFILIATED WITH A SPECIFIC LAW FIRM NOR DOES GET LAWCO OR LAWCO APP MAKE SPECIFIC CLAIM TO THE VALIDITY OF THE LEGAL ADVICE SOUGHT OR GIVEN. GET LAWCO AND THE LAWCO APP ARE MERELY CONNECTION VEHICLES BY WHICH PEOPLE MAY SEEK OUT LEGAL HELP AND ATTORNEYS MAY CONNECT TO THOSE IN NEED WITH REQUESTS. GET LAWCO AND THE LAWCO APP REVIEW SUBMISSIONS BY ATTORNEYS USING THE INFORMATION OBTAINED FROMd THE ILLINOIS ATTORNEY REGISTRATION AND DISCIPLINARY COMMITTEE.",
                "With respect to Third Party Providers, Charges you incur will be owed directly to Third Party Providers, and Get LawCo will not collect payment of those charges from you, on the Third Party Provider’s behalf and payment of the Charges shall be made directly by you, the non-attorney user or the attorney, to the Third Party Provider. In such cases, you retain the right to request lower Charges from a Third Party Provider for services or goods received by you from such Third Party Provider at the time you receive such services or goods and Charges you incur will be owed to the Third Party Provider, separate from Get LawCo. This payment structure is intended for you to fully compensate a Third Party Provider, if applicable, for the services obtained in connection with your use of their Services. In all cases, Charges you incur will be owed and paid directly to Third Party Providers."
              ]
            }
          ]
        },
        {
          title: "6. Disclaimers; Limitation of Liability; Indemnity",
          contents: [
            {
              title: "Disclaimer.",
              texts: [
                `THE SERVICES ARE PROVIDED "AS IS" AND "AS AVAILABLE." GET LAWCO DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED, OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN ADDITION, GET LAWCO MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE REGARDING THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, OR AVAILABILITY OF THE SERVICES OR ANY SERVICES REQUESTED THROUGH THE USE OF THE SERVICES, OR THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. GET LAWCO DOES NOT GUARANTEE THE QUALITY, SUITABILITY, SAFETY OR ABILITY OF THIRD PARTY PROVIDERS. YOU AGREE THAT THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE SERVICES, AND ANY SERVICE OR GOOD REQUESTED IN CONNECTION THEREWITH, REMAINS SOLELY WITH YOU, TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.`
              ]
            },
            {
              title: "Limitation of Liability.",
              texts: [
                "GET LAWCO SHALL NOT BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOST DATA, PERSONAL INJURY, OR PROPERTY DAMAGE RELATED TO, IN CONNECTION WITH, OR OTHERWISE RESULTING FROM ANY USE OF THE SERVICES, REGARDLESS OF THE NEGLIGENCE (EITHER ACTIVE, AFFIRMATIVE, SOLE, OR CONCURRENT) OF GET LAWCO, EVEN IF GET LAWCO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.",
                "GET LAWCO SHALL NOT BE LIABLE FOR ANY DAMAGES, LIABILITY OR LOSSES ARISING OUT OF: (i) YOUR USE OF OR RELIANCE ON THE SERVICES OR YOUR INABILITY TO ACCESS OR USE THE SERVICES; OR (ii) ANY TRANSACTION OR RELATIONSHIP BETWEEN YOU AND ANY THIRD PARTY PROVIDER, EVEN IF GET LAWCO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. GET LAWCO SHALL NOT BE LIABLE FOR DELAY OR FAILURE IN PERFORMANCE RESULTING FROM CAUSES BEYOND GET LAWCO'S REASONABLE CONTROL. YOU ACKNOWLEDGE THAT THIRD PARTY PROVIDERS PROVIDING SERVICES REQUESTED THROUGH SOME REQUEST PRODUCTS MAY OFFER SERVICES AND MAY NOT BE PROFESSIONALLY LICENSED.",
                "THE SERVICES MAY BE USED BY YOU TO REQUEST AND SCHEDULE SERVICES WITH THIRD PARTY PROVIDERS, BUT YOU AGREE THAT GET LAWCO HAS NO RESPONSIBILITY OR LIABILITY TO YOU RELATED TO ANY TRANSPORTATION, GOODS OR SERVICES PROVIDED TO YOU BY THIRD PARTY PROVIDERS OTHER THAN AS EXPRESSLY SET FORTH IN THESE TERMS.",
                "THE LIMITATIONS AND DISCLAIMER IN THIS SECTION DO NOT PURPORT TO LIMIT LIABILITY OR ALTER YOUR RIGHTS AS A CONSUMER THAT CANNOT BE EXCLUDED UNDER APPLICABLE LAW. BECAUSE SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF OR THE LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH STATES OR JURISDICTIONS, GET LAWCO’S LIABILITY SHALL BE LIMITED TO THE EXTENT PERMITTED BY LAW. THIS PROVISION SHALL HAVE NO EFFECT ON GET LAWCO’S CHOICE OF LAW PROVISION SET FORTH BELOW."
              ]
            },
            {
              title: "Indemnity.",
              texts: [
                "You agree to indemnify and hold Get LawCo and its affiliates and their officers, directors, employees, and agents harmless from any and all claims, demands, losses, liabilities, and expenses (including attorneys' fees), arising out of or in connection with: (i) your use of the Services or services obtained through your use of the Services; (ii) your breach or violation of any of these Terms; (iii) Get LawCo's use of your User Content; or (iv) your violation of the rights of any third party, including Third Party Providers."
              ]
            }
          ]
        },
        {
          title: "7. Other Provisions",
          contents: [
            {
              title: "Choice of Law.",
              texts: [
                "These Terms are governed by and construed in accordance with the laws of the State of Illinois, U.S.A., without giving effect to any conflict of law principles, except as may be otherwise provided in the Arbitration Agreement above or in supplemental terms applicable to your region. However, the choice of law provision regarding the interpretation of these Terms is not intended to create any other substantive right to non-Illinois residents to assert claims under Illinois law whether that be by statute, common law, or otherwise. These provisions, and except as otherwise provided in Section 2 of these Terms, are only intended to specify the use of Illinois law to interpret these Terms and the forum for disputes asserting a breach of these Terms, and these provisions shall not be interpreted as generally extending Illinois law to you if you do not otherwise reside in Illinois. The foregoing choice of law and forum selection provisions do not apply to the arbitration clause in Section 2 or to any arbitrable disputes as defined therein. Instead, as described in Section 2, the Federal Arbitration Act shall apply to any such disputes."
              ]
            },
            {
              title: "Claims of Copyright Infringement.",
              texts: [
                "Claims of copyright infringement should be sent to Get LawCo's designated agent."
              ]
            },
            {
              title: "Notice.",
              texts: [
                "Get LawCo may give notice by means of a general notice on the Services, electronic mail to your email address in your Account, telephone or text message to any phone number provided in connection with your account, or by written communication sent by first class mail or pre-paid post to any address connected with your Account. Such notice shall be deemed to have been given upon the expiration of 48 hours after mailing or posting (if sent by first class mail or pre-paid post) or 12 hours after sending (if sent by email or telephone). You may give notice to Get LawCo, with such notice deemed given when received by Get LawCo, at any time by first class mail or pre-paid post to our registered agent for service of process, c/o Get LawCo, LLC"
              ]
            },
            {
              title: "General.",
              texts: [
                "You may not assign these Terms without Get LawCo's prior written approval. Get LawCo,LLC may assign these Terms without your consent to: (i) a subsidiary or affiliate; (ii) an acquirer of Get LawCo's equity, business or assets; or (iii) a successor by merger. Any purported assignment in violation of this section shall be void. No joint venture, partnership, employment, or agency relationship exists between you, Get LawCo or any Third Party Provider as a result of this Agreement or use of the Services. If any provision of these Terms is held to be invalid or unenforceable, such provision shall be struck and the remaining provisions shall be enforced to the fullest extent under law. Get LawCo's failure to enforce any right or provision in these Terms shall not constitute a waiver of such right or provision unless acknowledged and agreed to by Get LawCo in writing. This provision shall not affect the Severability and Survivability section of the Arbitration Agreement of these Terms."
              ]
            },
            {
              title: "Attorneys.",
              texts: [
                "Attorneys interested in becoming an authorized attorney to use the LawCo App, are required to register with the LawCo App, to take a picture of their current and valid Illinois ARDC card and upload it to the LawCo App. By taking a picture of their current and valid Illinois ARDC card, the attorney is attesting to the truth of their submission; that they are indeed the person as registered, and that they are the registered person currently authorized by the Illinois ARDC to practice law. The attorney, by submission of this information, is also under the affirmative obligation and duty to inform Get LawCo, LLC, of any change in their status as a licensedand registered attorney, in writing, via certified mail to Get LawCo, LLC, 9925 S. Seeley Avenue, Chicago, Illinois 60643. It is hereby further stated that Get LawCo, LLC, has the discretion to deny any attorney authorization of use of the LawCo App. Get LawCo, LLC, reserves the right to deny or authorize the attorney on acase by case basis. Even if the attorney is currently registered with the Illinois ARDC and currently licensed to practice law in Illinois bythe Illinois ARDC, Get LawCo, LLC, reserves the right to deny the attorney authorization to the LawCo App, thereby denyingthe attorney approval; if said attorney has a prior history of discipline with the Illinois ARDC.",
                "Attorneys must comply with all applicable state, federal and local laws. An attorney’s violation of the laws governing the practice of law or violation of any applicable law, constitutes a breach of the Terms and Conditions of Get LawCo, LLC. Any violations of law may be reported to both the Illinois ARDC and the proper authorities.",
                "Attorneys are expected to accommodate leads using walkers, canes, folding wheelchairs or other assistive devices to the maximum extent feasible.",
                "Any report of unlawful discrimination will result in the temporary deactivation of the attorney’s account while Get LawCo, LLC, reviews the incident. Confirmed violations of the law with respect to discrimination may result in permanent loss of an attorney’s access to the LawCo App."
              ]
            },
            {
              title: "Pricing and Refund Policy.",
              texts: [
                "Facilitation of Charges. All Charges are facilitated through a third-party payment processing service (e.g., Stripe, Inc., or Braintree, a division of PayPal, Inc.). Get LawCo, LLC,may replace its third-party payment processing services without notice to you. Charges shall only be made through the LawCo App platform. Cash payments are strictly prohibited. Your payment of Charges to Get LawCo, LLC, satisfies your payment obligation for your use of the LawCo App platform and services.",
                "No Refunds. All Charges are non-refundable. This no-refund policy shall apply at all times regardless of your decision to terminate usage of the LawCo, App, any disruption to the LawCo App platform or services, or any other reason whatsoever.",
                "Promo Codes. You may receive coupons that you can apply toward payment of certain Charges upon completion of a Lead. Promo Codes are only valid for use on the LawCo App platform, and are not transferable or redeemable for cash. Promo Codes cannot be combined, and if the cost of your lead connection exceeds the applicable credit or discount value, Get LawCo, LLC, will charge your payment method on file for the outstanding cost of the lead connection. Additional restrictions on promo codes may apply as communicated to you in a relevant promotion or by clicking on the relevant promo codes within the advertisement.",
                "Credit Card Authorization. Upon addition of a new payment method or each ride request, Get LawCo, LLC, may seek authorization of your selected payment method to verify the payment method, ensure the lead generation fee will be covered, and protect against unauthorized behavior. The authorization is not a charge, however, it may reduce your available credit by the authorization amount until your bank’s next processing cycle. Should the amount of our authorization exceed the total funds on deposit in your account, you may be subject to overdraft of NSF charges by the bank issuing your debit or prepaid card. We cannot be held responsible for these charges and are unable to assist you in recovering them from your issuing bank."
              ]
            },
            {
              title: "Lead cancellations and no-shows.",
              texts: [
                "If you cancelled a lead for safety reasons, please call 911.",
                "If you cancelled the lead for safety reasons and already paid the lead generation fee, you can earn a lead generation fee return if you contact: admin@getlawco.com within 24 hours of the cancellation. Get LawCo, LLC, reserves the right to issue lead generation fee returns on a case by case basis.",
                [
                  "Requesting lead's name",
                  "Time and date of lead acceptance",
                  "Lead location",
                  "Reason for cancellation"
                ],
                "If a lead cancels or no-shows when you've already accepted and paid for the lead generation fee, you can earn a lead generation fee return if you contact: admin@getlawco.com within 24 hours of the cancellation or no-show. Get LawCo, LLC, reserves the right to issue lead generation fee returns on a case by case basis.",
                "Include the followinging your email:",
                [
                  "Requesting lead's name",
                  "Time and date of lead acceptance",
                  "Lead location",
                  "Reason for cancellation"
                ],
                "Please note Get LawCo, LLC, reserves the right to withhold, deduct, or reduce the amount of any cancellation fee returns at our discretion, including if fees are determined or believed to result from error, fraud, or a violation of the Get LawCo, LLC, terms and conditions."
              ]
            }
          ]
        }
      ]
    },
    privacyPolicy: {
      title: 'Privacy Policy'
    },
    profile: {
      yourRequests: 'Your requests',
      submitted: 'Submitted',
      closedBy: 'Closed by {lawyerName}',
      acceptedBy: 'Accepted by {lawyerName}',
      noRequests: 'There are no requests to display.'
    }
  },
  userInfo: {
    firstName: "First Name",
    lastName: "Last Name",
    fullName: "Last Name",
    email: "Email",
    userEmail: "Your email address",
    password: "Password",
    confirmPassword: "Confirm password",
    passwordMismatch: "Passwords do not match",
    ardcNumber: "ARDC Number",
    accountType: "Account Type",
    location: "Location",
    lawyer: "lawyer",
    client: "client",
    consumers: 'consumers',
    signedout: "Signed Out",
    phoneNumber: 'Phone Number',
    phone: 'Phone',
    preferredLocations: 'Preferred Locations',
  },
  jobRequest: {
    case: "Case",
    courtDate: "Court date",
    legalAdvice: "Legal Advice",
    location: "Location",
    courtLocation: "Court Location",
    date: "Date",
    time: "Time",
    category: "Category",
    areaOfLegalAdvice: "Area of Legal Advice",
    legalNeeds: "Legal Needs",
    lnPlaceholder: "Briefly describe your legal needs, but don't tell us your secrets here.",
    caseType: "Type",
    preferredContactMethod: "Preferred Method of Contact",
    contactMethod: {
      userEmail: 'Email',
      phone: 'Phone',
    },
    status: {
      pending: "Requests",
      leads: "Leads",
      closedLeads: "Closed Leads"
    },
    hasCourtDate: 'This request has a court date.',
    consents: [
      {
        title: "You are Asking to be Contacted by an Attorney.",
        description: "By entering information about your legal matter into the LawCo App, you are requesting that an attorney contact you through the LawCo App in order to provide information in response to your request as well as information about the attorney’s terms of representation.*"
      },
      {
        title: "LawCo Does not Choose the Attorney Who Contacts you.",
        description: "Once your request is submitted, any attorney using the LawCo App may contact you. LawCo does not make any representations or warranties as to the qualifications of an attorney who contacts you. If you do not feel an attorney is qualified, you may resubmit your request in order to be contacted by another attorney.*"
      },
      {
        title: "No Attorney-Client Relationship Yet.",
        description: "I know that just because I fill out this online form, no lawyer has taken my case at this point.*"
      }
    ],
    readAndAccept: "I have read and accepted {termsOfUse} and {privacyPolicy}.*",
    termsOfUse: "the terms of use",
    privacyPolicy: "privacy policy",
  },
  menu: {
    home: "Home",
    dashboard: "Admin Dashboard",
    lawyerApplications: "Lawyer Applications",
    clients: "Clients",
    lawyers: "Lawyers",
    requests: "Requests",
    faq: "Frequently Asked Questions",
    tac: "Terms and Conditions",
    contact: "Contact LawCo",
    logout: "Log Out",
    profile: "Profile",
    utils: "Utilities",
    chat: "Chat"
  },
  payment: {
    overview: "Payment Overview",
    info: "Payment Information",
    leadGenFee: "Lead Generation Fee",
    total: "Total Due",
    cardNumber: "Card Number",
    expDate: "Exp. Date",
    cvc: "CVC/CVV",
    pay: "Pay {total}"
  },
  promoCode: {
    name: "Promo Code | Promo Codes",
    promoCodeName: "Promo Code Name",
    eventStart: "Start Date",
    eventEnd: "End Date",
    discountPercentage: "Discount Percentage",
    discount: "Discount",
    status: {
      active: "Active",
      expired: "Expired"
    },
    deleteConfirmMessage: 'Are you sure you want to delete this promo code?',
  },
  location: {
    title: "Locations",
    name: 'Location Name',
    address1: "Address line 1",
    address2: "Address line 2",
    city: "City",
    state: "State",
    zip: "Zip"
  },
  chat: {
    threads: "Threads",
    messages: "Messages",
    noActiveThreads: "No active threads",
    selectThread: "Select thread...",
    startConversation: "Start conversation...",
    closedCaseInfo: "Chat is disabled for closed cases."
  },
  auth: {
    noAccess: "You have not enough permissions to access this page",
    notApproved: "Your account is not approved yet, please try again later"
  },
  field: {
    required: {
      one: "{0} is required", // First Name is required
      all: "Please fill out all required fields",
      ardc: "You have not added your ARDC number. Please update your profile.",
      consent: "Consent is required",
      tac: 'Please accept terms and conditions',
      checkbox: 'Please tick the checkbox to continue',
      preferredLocations: 'At least one location is required',
    },
    invalid: "Invalid {0}", // Invalid Start Date, Invalid end date, etc
    dateInPast: 'Date cannot be in past',
    invalidDate: 'Date is not valid',
    invalidTime: 'Time is not valid',
  },
  actions: {
    prompt: "Are you sure?",
    save: "Save {0}", // Save Court date, Save date, etc
    updateProfile: "Update Profile",
    editProfile: "Edit Profile",
    closeLead: "Close Lead",
    acceptRequest: "Accept Request",
    promoCode: "Add Promo Code",
    editPromoCode: "Edit Promo Code",
    updatePromoCode: "Update Promo Code",
    deletePromoCode: "Delete Promo Code",
    addLocation: "Add Location",
    updateLocation: "Update Location",
    resetPassword: "set new password",
    requestLegalAssistance: "Request legal assistance",
    next: "next",
    back: "back",
    cancel: "cancel",
    signin: "Sign In",
    signup: "Sign Up",
    signout: "Sign Out",
    approve: "Approve",
    reject: "Reject",
    accept: "Accept",
    decline: "Decline",
    updatePreferredLocations: "Click HERE to update your profile to select your preferred courthouse location(s)!",
  },
  success: {
    submitted: 'Your request has been submitted.',
    courtDate: "Your case is added and waits for approval",
    case: "Your case is added and waits for approval",
    profile: "Successfully updated profile",
    closeLead: "Successfully closed lead.",
    payment: "Payment Successful.",
    resetPassword: "New password saved!",
    promoCode: "Promo Code added",
    promoCodeUpdate: "Promo Code updated",
    promoCodeDelete: "Promo Code deleted",
    updateLawyerApplication: "Lawyer application {status}",
    addLocation: "Location added",
  },
  error: "Something went wrong, please try again.", // default eror message,
  noResult: 'There are no results to be displayed.'
}